package ru.sber.spring.libraryproject.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.libraryproject.dto.AuthorDTO;
import ru.sber.spring.libraryproject.model.Author;
import ru.sber.spring.libraryproject.service.AuthorService;


@RestController
@RequestMapping("/authors")
@Tag(name = "Авторы",
        description = "Контроллер для работы с авторами книг библиотеки")
public class AuthorController
        extends GenericController<Author, AuthorDTO> {

    private AuthorService authorService;


    public AuthorController(AuthorService authorService) {
        super(authorService);
        this.authorService = authorService;
    }

}
