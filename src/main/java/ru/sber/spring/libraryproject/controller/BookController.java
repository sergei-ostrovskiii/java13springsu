package ru.sber.spring.libraryproject.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.*;
import ru.sber.spring.libraryproject.dto.BookDTO;
import ru.sber.spring.libraryproject.model.Book;
import ru.sber.spring.libraryproject.service.BookService;

@RestController
@RequestMapping(value = "/books")
@Tag(name = "Книги",
        description = "Контроллер для работы с книгами библиотеки")
public class BookController extends GenericController<Book, BookDTO> {
    public BookController(BookService bookService) {
        super(bookService);
    }
}
