package ru.sber.spring.libraryproject.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.libraryproject.dto.BookRentInfoDTO;
import ru.sber.spring.libraryproject.model.BookRentInfo;
import ru.sber.spring.libraryproject.service.BookRentInfoService;


@RestController
@RequestMapping("/rent/info")
@Tag(name = "Аренда книг",
        description = "Контроллер для работы с арендой/выдачей книг пользователям библиотеки")
public class RentBookController
        extends GenericController<BookRentInfo, BookRentInfoDTO> {
    public RentBookController(BookRentInfoService bookRentInfoService) {
        super(bookRentInfoService);
    }
}

