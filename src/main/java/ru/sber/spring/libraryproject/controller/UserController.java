package ru.sber.spring.libraryproject.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sber.spring.libraryproject.dto.UserDTO;
import ru.sber.spring.libraryproject.model.User;
import ru.sber.spring.libraryproject.service.UserService;

@RestController
@RequestMapping("/users")
@Tag(name = "Пользователи",
        description = "Контроллер для работы с пользователями библиотеки")
public class UserController
        extends GenericController<User, UserDTO> {

    public UserController(UserService userService) {
        super(userService);
    }
}


