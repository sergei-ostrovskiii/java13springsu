package ru.sber.spring.libraryproject.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.sber.spring.libraryproject.model.Genre;
import java.time.LocalDate;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class BookDTO extends GenericDTO {
    private String title;
    private LocalDate publishDate;
    private String publish;
    private Integer pageCount;
    private String storagePlace;
    private String onlineCopy;
    private String description;
    private Integer amount;
    private Genre genre;
    private Set<Long> authorsIds;

}
