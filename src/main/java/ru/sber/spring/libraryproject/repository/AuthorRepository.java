package ru.sber.spring.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.libraryproject.model.Author;
@Repository
public interface AuthorRepository
        extends GenericRepository<Author> {
}
