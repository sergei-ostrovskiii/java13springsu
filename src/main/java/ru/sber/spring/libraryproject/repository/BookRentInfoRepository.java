package ru.sber.spring.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.libraryproject.model.BookRentInfo;

@Repository
public interface BookRentInfoRepository extends GenericRepository<BookRentInfo>{
}
