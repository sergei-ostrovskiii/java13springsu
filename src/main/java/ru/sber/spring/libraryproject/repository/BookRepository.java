package ru.sber.spring.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.libraryproject.model.Book;
@Repository
public interface BookRepository extends GenericRepository<Book>{
}

