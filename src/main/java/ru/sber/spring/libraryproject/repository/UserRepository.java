package ru.sber.spring.libraryproject.repository;

import org.springframework.stereotype.Repository;
import ru.sber.spring.libraryproject.model.User;
@Repository
public interface UserRepository extends GenericRepository<User>{
}
