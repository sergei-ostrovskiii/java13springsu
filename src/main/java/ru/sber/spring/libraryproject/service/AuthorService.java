package ru.sber.spring.libraryproject.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.libraryproject.dto.AuthorDTO;
import ru.sber.spring.libraryproject.mapper.AuthorMapper;
import ru.sber.spring.libraryproject.model.Author;
import ru.sber.spring.libraryproject.repository.AuthorRepository;
@Service
public class AuthorService
        extends GenericService<Author, AuthorDTO> {
    protected AuthorService(AuthorRepository authorRepository,
                            AuthorMapper authorMapper) {
        super(authorRepository, authorMapper);
    }
}
