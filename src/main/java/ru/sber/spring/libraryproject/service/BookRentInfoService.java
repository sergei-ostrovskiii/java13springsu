package ru.sber.spring.libraryproject.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.libraryproject.dto.BookRentInfoDTO;
import ru.sber.spring.libraryproject.mapper.BookRentInfoMapper;
import ru.sber.spring.libraryproject.model.BookRentInfo;
import ru.sber.spring.libraryproject.repository.BookRentInfoRepository;

@Service
public class BookRentInfoService
        extends GenericService<BookRentInfo, BookRentInfoDTO> {
    private BookRentInfoRepository bookRentInfoRepository;

    protected BookRentInfoService(BookRentInfoRepository bookRentInfoRepository,
                                  BookRentInfoMapper bookRentInfoMapper) {
        super(bookRentInfoRepository, bookRentInfoMapper);
        this.bookRentInfoRepository = bookRentInfoRepository;
    }
}


