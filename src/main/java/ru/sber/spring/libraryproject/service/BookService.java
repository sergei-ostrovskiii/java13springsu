package ru.sber.spring.libraryproject.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.libraryproject.dto.BookDTO;
import ru.sber.spring.libraryproject.mapper.BookMapper;
import ru.sber.spring.libraryproject.model.Book;
import ru.sber.spring.libraryproject.repository.BookRepository;
@Service
public class BookService
        extends GenericService<Book, BookDTO> {
    //  Инжектим конкретный репозиторий для работы с таблицей books
    private final BookRepository repository;

    protected BookService(BookRepository repository,
                          BookMapper mapper) {
        //Передаем этот репозиторй в абстрактный севрис,
        //чтобы он понимал с какой таблицей будут выполняться CRUD операции
        super(repository, mapper);
        this.repository = repository;
    }
}

