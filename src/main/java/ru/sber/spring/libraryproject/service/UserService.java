package ru.sber.spring.libraryproject.service;

import org.springframework.stereotype.Service;
import ru.sber.spring.libraryproject.dto.RoleDTO;
import ru.sber.spring.libraryproject.dto.UserDTO;
import ru.sber.spring.libraryproject.mapper.UserMapper;
import ru.sber.spring.libraryproject.model.User;
import ru.sber.spring.libraryproject.repository.UserRepository;

@Service
public class UserService
        extends GenericService<User, UserDTO> {

    protected UserService(UserRepository userRepository,
                          UserMapper userMapper) {
        super(userRepository, userMapper);
    }

    @Override
    public UserDTO create(UserDTO object) {
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setId(1L);
        object.setRole(roleDTO);
        return mapper.toDto(repository.save(mapper.toEntity(object)));
    }
}


